# lua
Fork of Lua for simple building with CMake. Embedded only.

Windows
-------
If you have CMake in ``PATH`` environment:
```
mkdir build
cd build
cmake ..
cmake --build .
```
